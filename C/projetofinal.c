#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct string {
	char nome_produto[30];
};

//variaveis
struct string lista_produtos[100];
int codigo[100], un_kg[100];
float preco[100];

int n_produtos=-1,i,busca,produto_buscado, opcao_menu_produtos, opcao_realizar_compra=0;
bool opcao_valida;
char opcao[25], eh_preco[25], eh_codigo[25];
float preco_total,subtotal,troco;


int menu_produtos();
int cadastrar_produto();
int relatorio_produtos();
int buscar_produtos();
int alterar();
int excluir();
int menu_realizar_compra();
int comprar_por_codigo();
int finalizar_compra();

int mostrar_produto();
bool procurar_codigo();
bool verifica_se_eh_preco();
bool verifica_se_eh_codigo();
bool verifica_se_eh_opcao(char ate);


//menus----------------------------------------------------------------------------

int main(){
	system("clear");
	printf("**Bem vindo!!!**\n\n");
	printf("Digite uma das opções para continuar:\n");
	printf("\t1- Realizar compra\n");
	printf("\t2- Ir para menu de produtos\n");
	printf("\t3- Sair\n\t\t");
	scanf("%s",opcao);
	setbuf(stdin,NULL);	

	if(strlen(opcao)!=1){
		main();
		return -1;
	}

	switch(opcao[0]){
		case '1':
			preco_total=0;
			subtotal=0;
			menu_realizar_compra();
			break;
		case '2':
			menu_produtos();
			break;
		case '3':
			break;
		default:
			main();
			break;
	}
}


int menu_produtos(){
	system("clear");
	switch (opcao_menu_produtos){
		case 1:
			printf("Produto cadastrado com sucesso!\n\n");	
			break;
		case 2:
			printf("Produto não cadastrado\n\n");
			n_produtos--;
			break;
		case 3:
			printf("Produto excluído com sucesso!\n\n");
			break;
	}
	opcao_menu_produtos=0;

		printf("**Menu de produtos**\n\n");
		printf("Escolha uma das opções:\n");
		printf("\t1- Cadastrar produto\n");
		printf("\t2- Buscar produto\n");
		printf("\t3- Gerar relatório com todos os produtos\n");
		printf("\t4- Voltar ao menu inicial\n\t\t");

		scanf("%s",opcao);
		setbuf(stdin,NULL);

		if(strlen(opcao)!=1){
			menu_produtos();
			return -1;
		}

		switch(opcao[0]){
		case '1':
			cadastrar_produto();
			break;
		case '2':
			buscar_produtos();
			break;
		case '3':
			relatorio_produtos();
			break;
		case '4':
			main();
			break;
		default:
			menu_produtos();
			break;
	}
}


int cadastrar_produto(){
	system("clear");
	printf("**Cadastro de produto**\n\n");

	do{
		printf("Digite o código do produto: ");
		scanf("%s",eh_codigo);
		setbuf(stdin,NULL);
	}while(verifica_se_eh_codigo()==false);

	busca=atoi(eh_codigo);

	if(procurar_codigo()==true){
		printf("\nJá existe um produto cadastrado com esse código\n");
		printf("Pressione enter para voltar\n");
		getchar();
		menu_produtos();
		return -1;
	}
	n_produtos++;

	codigo[n_produtos]=busca;

	printf("Digite o nome do produto: ");
	scanf("%[^\n]s",lista_produtos[n_produtos].nome_produto);
	setbuf(stdin,NULL);



	do{
		printf("Escolha uma das opções:\n");
		printf("\t1- Vendido por unidade\n");
		printf("\t2- Vendido por kilo\n\t\t");
		scanf("%[^\n]s",opcao);
	}while(verifica_se_eh_opcao('2')==false);

	un_kg[n_produtos]=atoi(opcao);
	
	do{
		if(un_kg[n_produtos]==1){
			printf("Digite o preço por unidade(R$): ");
		}
		else{
			printf("Digite o preço por kilo(R$): ");
		}
		scanf("%s",eh_preco);
	}while(verifica_se_eh_preco()==false);
	preco[n_produtos]=atof(eh_preco);
	
	
	do{
		printf("\n\nDeseja salvar?\n");
		printf("Escolha uma das opções:\n");
		printf("\t1- Sim\n");
		printf("\t2- Não\n\t\t");
		scanf("%[^\n]s",opcao);
	}while(verifica_se_eh_opcao('2')==false);
	opcao_menu_produtos=atoi(opcao);

	menu_produtos();
}


int buscar_produtos(){
	system("clear");
	printf("**Buscar produtos**\n\n");

	do{
		printf("Digite o código do produto que deseja buscar: ");
		scanf("%[^\n]s",eh_codigo);
	}while(verifica_se_eh_codigo()==false);

	busca=atoi(eh_codigo);

	if (procurar_codigo()==false){
		printf("\nNão encontrado!\n");
		printf("\nPressione enter para voltar\n");
		getchar();
		menu_produtos();
		return -1;
	}	

	produto_buscado=i;
	printf("\nEncontrado!\n");
	mostrar_produto(produto_buscado);
	do{
		printf("\nO que deseja fazer?\n\n");
		printf("Escolha uma das opções:\n");
		printf("\t1-Alterar informações do produto\n");
		printf("\t2-Excluir produto\n");
		printf("\t3-Voltar\n\t\t");
		scanf("%s",opcao);
	}while(verifica_se_eh_opcao('3')==false);

	switch(opcao[0]){
		case '1':
			alterar();
			break;
		case '2':
			excluir();
			break;
		case '3':
			menu_produtos();
			break;
	}
}

int relatorio_produtos(){
	system("clear");
	printf("**Relatório:**\n");
	if(n_produtos==-1){
		printf("\nNenhum produto cadastrado");
	}
	for(i=0;i<=n_produtos;i++){
		mostrar_produto(i);
	}
	printf("\nPressione enter para voltar");
	getchar();
	menu_produtos();
}


int alterar(){
	system("clear");
	printf("**Alterar produto**\n");
	mostrar_produto(produto_buscado);
	
	do{
		printf("\nO que deseja alterar?\n\n");
		printf("Escolha uma das opções:\n");
		printf("\t1-Alterar código\n");
		printf("\t2-Alterar descrição\n");
		printf("\t3-Alterar preço\n");
		printf("\t4-Voltar\n\t\t");
		scanf("%s",opcao);
	}while(verifica_se_eh_opcao('4')==false);

	switch(opcao[0]){
		case '1':
			do{
				do{
					printf("Digite o novo código do produto: ");
					scanf("%s",eh_codigo);
				}while(verifica_se_eh_codigo()==false);

				busca=atoi(eh_codigo);
				if(procurar_codigo()==true){
						printf("\nJá existe um produto cadastrado com esse código\n");
				}
			}while(procurar_codigo()==true);

			codigo[produto_buscado]=busca;
			break;
		case '2':
			printf("Digite o novo nome do produto: ");
			getchar();
			scanf("%[^\n]s",lista_produtos[produto_buscado].nome_produto);
			setbuf(stdin,NULL);
			break;
		case '3':
			do{
				printf("Escolha uma das opções:\n");
				printf("\t1- Vendido por unidade\n");
				printf("\t2- Vendido por kilo\n\t\t");
				scanf("%s",opcao);
			}while(verifica_se_eh_opcao('2')==false);

			do{
				if(un_kg[produto_buscado]==1){
					printf("Digite o novo preço por unidade(R$): ");
				}
				else{
					printf("Digite o novo preço por kilo(R$): ");
				}
				scanf("%s",eh_preco);
			}while(verifica_se_eh_preco()==false);
			preco[produto_buscado]=atof(eh_preco);
			break;
		case '4':
			menu_produtos();
			break;
		}
		printf("\nAlteração concluída com sucesso!");
		printf("\n\n Pressione enter para voltar");
		setbuf(stdin,NULL);
		getchar();
		menu_produtos();	
}

int excluir(){
	system("clear");
	printf("**Excluir produto**\n");
	
	mostrar_produto(produto_buscado);

	do{
		printf("\n\nTem certeza que deseja excluir?");
		printf("\nEscolha uma das opções:\n");
		printf("\t1- Sim\n");
		printf("\t2- Não\n\t\t");
		scanf("%s",opcao);
	}while(verifica_se_eh_opcao('2')==false);

	if(opcao[0]=='1'){
		for(i;i<n_produtos;i++){
			codigo[i]=codigo[i+1];
			strcpy(lista_produtos[i].nome_produto,lista_produtos[i+1].nome_produto);
			un_kg[i]=un_kg[i+1];
			preco[i]=preco[i+1];
		}
	n_produtos--;
	opcao_menu_produtos=3;
	}

	menu_produtos();
}


int menu_realizar_compra(){
	if(n_produtos==-1){
		printf("\nNenhum produto cadastrado");
		printf("\nPressione enter para voltar");
		getchar();
		main();
		return -1;
	}
	system("clear");
	switch(opcao_realizar_compra){
		case 1:
			printf("Adicionado\n");
			preco_total=preco_total+subtotal;
			break;
		case 2:
			printf("Descartado\n");
			break;
		case 3:
			printf("Código não encontrado\n");
			break;


	}
	printf("**Realizar compra**\n\n"); 
	
	do{
		printf("Subtotal: R$ %.2f\n\n",preco_total);
		printf("O que deseja fazer?\n");
		printf("Digite uma das opções\n");
		printf("\t1- Continuar comprando\n");
		printf("\t2- Finalizar compra\n\t\t");
		scanf("%s",opcao);
	}while(verifica_se_eh_opcao('2')==false);
		switch(opcao[0]){
			case '1':
				comprar_por_codigo();
				break;
			case '2':
				finalizar_compra();
				break;
		}
}

					
int comprar_por_codigo(){
	system("clear");
	do{
		printf("Digite o código do produto:\n");
		scanf("%s",eh_codigo);
	}while(verifica_se_eh_codigo()==false);

	busca=atoi(eh_codigo);

	if(procurar_codigo()==false){
		opcao_realizar_compra=3;
		menu_realizar_compra();
		return -1;
	}
	produto_buscado=i;
	mostrar_produto(produto_buscado);


	do{
		if(un_kg[produto_buscado]==1){
			printf("\nDigite a quantidade em unidades: ");
		}
		else{
			printf("\nDigite a quantidade em kilos: ");
		}
		scanf("%s",eh_preco);
	}while(verifica_se_eh_preco()==false);
	subtotal=atof(eh_preco);
	subtotal=preco[produto_buscado]*subtotal;
	printf("\nValor: %.2f\n",subtotal);

	do{
		printf("\nDeseja adicionar?\n");
		printf("Digite uma das opções\n");
		printf("\t1- Sim\n");
		printf("\t2- Não\n");
		scanf("%s",opcao);
	}while(verifica_se_eh_opcao('2')==false);

	opcao_realizar_compra=atoi(opcao);
	menu_realizar_compra();
}

int finalizar_compra(){
	system("clear");
	printf("**Finalizar compra**\n");

	if(preco_total==0){
		printf("\nNão foi comprado nada!\n");
		printf("Pressione enter para voltar ao menu inicial");
		getchar();
		main();
		return -1;
	} 
	printf("\nTotal: R$ %.2f\n\n",preco_total);
	
	do{
		printf("\nDeseja confirmar a compra?");
		printf("\nDigite uma das opções");
		printf("\n\t1- Sim");
		printf("\n\t2- Não\n\t\t");
		scanf("%s",opcao);
	}while(verifica_se_eh_opcao('2')==false);

	if(opcao[0]=='2'){
		main();
		return -1;
	}

	do{
		do{
			printf("\nDigite o valor pago (R$): ");
			scanf("%s",eh_preco);
		}while(verifica_se_eh_preco()==false);
		troco=atof(eh_preco);
		troco=troco - preco_total;
		if(troco<0){
			printf("Valor insuficiente\n");
		}
	}while(troco<0);

	printf("\nValor do troco (R$): %.2f\n",troco);
	printf("\nPressione enter para voltar ao menu inicial");
	getchar();
	main();
}


//funçoes de utilidade --------------------------------------------------------------

bool procurar_codigo(){
	for(i=0;i<=n_produtos;i++){
		if(busca==codigo[i]){
			return true;
		}
	}
	return false;
} 

int mostrar_produto(int produto){
	printf("\nCódigo: %d",codigo[produto]);
	printf("\nDescrição: %s", lista_produtos[produto].nome_produto);
	if(un_kg[produto]==1){
		printf("\nPreço: R$%.2f (unidade)",preco[produto]);
	}
	else{
		printf("\nPreço: R$%.2f (kg)",preco[produto]);
	}
	printf("\n");
}


//funções de strings-------------------------------------------------------------------
bool verifica_se_eh_opcao(char ate){ 
	setbuf(stdin,NULL);
		if(strlen(opcao)==1 & opcao[0] >= '1' & opcao[0]<= ate ){
			return true;
		}
	printf("Opção inválida.\n");
	return false;
}

bool verifica_se_eh_preco(){
	setbuf(stdin,NULL);	
	int ponto=0;
	for(int i=0; i<strlen(eh_preco);i++){
		if(!(eh_preco[i] >= '0' & eh_preco[i]<= '9' )){
			if(eh_preco[i]==','){
				eh_preco[i]='.';
			}
			if(eh_preco[i]=='.'){
				ponto++;
			}
			else{
				printf("Valor inválido\n");
				return false;
			}
			if(ponto==2){
				("Valor inválido\n");
				return false;		
			}
		}
	}
	return true;
}

bool verifica_se_eh_codigo(){
	setbuf(stdin,NULL);	
	for(int i=0; i<strlen(eh_codigo);i++){
		if(!(eh_codigo[i] >= '0' & eh_codigo[i]<= '9' )){
			printf("Atenção: Apenas números!\n");
			return false;
		}
	}
	return true;
}