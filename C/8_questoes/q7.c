//Obs: Apesar da questão nao pedir ṕara mostrar a quantidade que o vendedor recebe,
// optei por mostrar, visto que o valor do salário estava sendo inutilizado

#include <stdio.h>

//declaro a constante n
//n corresponde a quantidade de produtos da loja;
#define n 10 

int main()
{
    float valorUnitario[n];
    int quantidadeVendida[n], i;
    char nomeVendedor[20];

    //lendo o nome do vendedor
    printf("Digite o nome do vendedor:\n");
    scanf("%s", nomeVendedor);

    //preenchendo os vetores com os valores digitados pelo usuario
    for (i = 0; i < n; i++)
    {
        printf("Digite o valor unitario do produto %d: ", i + 1);
        scanf("%f", &valorUnitario[i]);
        printf("Digite a quantidade vendida do produto %d: ", i + 1);
        scanf("%d", &quantidadeVendida[i]);
    }

//A variavel maisVendido ira receber a quantidade do produto mais vendido
int maisVendido = 0;
//A variavel posicao irá receber a posicao do produto mais vendido
int posicaoMaisVendido;
//A variável soma geral recebe o valor total vendido de todos os produtos
float somaGeral = 0;
    for (i = 0; i < n; i++)
    {
        printf("\nProduto %d\n", i + 1);
        printf("Vendedor: %s:\n", nomeVendedor);
        printf("Quantidade vendida: %d\n", quantidadeVendida[i]);
        printf("Valor unitário: R$ %.2f\n", valorUnitario[i]);
        printf("Valor total vendido: R$ %.2f\n", valorUnitario[i] * quantidadeVendida[i]);
        somaGeral += valorUnitario[i] * quantidadeVendida[i];
        //se a quantidade vendida do produto i for maior que o valor armazenado em maisVendido
        if(quantidadeVendida[i] > maisVendido){
            //maisVendido recebe o novo maior valor
            maisVendido = quantidadeVendida[i];
            posicaoMaisVendido = i;
        }
    }
    //calculo do bonus do vendedor
    //a variavel bonus é inteira para receber somente o valor inteiro da divisao da somaGeral por 1000
    int bonus = somaGeral / 1000;
    //deste modo, a cada 1000 reais, o vendedor recebe 25
    bonus = bonus * 25;

    //a variavel comissão recebe a comissão do vendedor
    float comissao = 5.5*somaGeral/100;

    printf("\nValor geral das vendas: R$ %.2f\n", somaGeral);
    printf("Valor do bonus: R$ %d\n", bonus);
    printf("Valor da comissao: R$ %.2f\n", comissao);
    //valor a receber é a soma do salario, do bonus e da comissao
    printf("Valor a receber: R$ %.2f\n\n", 1058.76 +bonus + comissao);

    //Imprimi o valor do objeto que teve maior venda
    printf("Valor do objeto mais vendido: R$ %.2f\n", valorUnitario[posicaoMaisVendido]);
    //Imprimi a posicao do objeto que teve maior venda
    printf("Sua posição no vetor: %d", posicaoMaisVendido);
   
}
